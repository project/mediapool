<?php

function mediapool_settings_form($form_state) {
}

function mediapool_statistics_form_submit($form, &$form_state) {
}

function mediapool_statistics_form($form_state) {
//  drupal_add_css(drupal_get_path('module', 'imagepool') .'/css/imagepool.css');

// make a list of the imagepool pools
  if (module_exists('imagepool')) {
      $query = db_query("SELECT n.nid, type, nr.title, status, body FROM {node} n JOIN {node_revisions} nr ON (n.vid=nr.vid) WHERE type='imagepool'");
      if ($query) {
        while ($result = db_fetch_object($query)) {
          $pools[$result->nid] = $result;
          $query2 = db_query("SELECT name FROM {term_data} td JOIN {term_node} tn ON (tn.tid=td.tid) WHERE tn.nid=" . $result->nid);
          if ($query2) {
            $t = array();
            while ($result2 = db_fetch_object($query2)) {
              $t[] = $result2->name;
            }
            $terms[$result->nid] = implode(',',$t);
          }
        }
        if ($pools) {
          $output = _mediapool_statistics_generate($pools, $terms);
          print $output;
        }
      }
  }
  return $form;
}

function _mediapool_statistics_generate($pools, $terms) {
  $output .= '<table id="mediapool">';
  $output .= '<tr id="mediapool-heading" class="mediapool">' . _mediapool_statistics_generate_column_headings() . '</tr>';
  foreach ($pools as $nid => $pool) {
    $output .= '<tr class="mediapool mediapool-row "' . (((++$ctr)%2) ? 'odd' : 'even') . '>' . _mediapool_statistics_generate_row($pool, $terms[$nid]) . '</tr>';
  }
  
  return $output;   
}

function _mediapool_statistics_generate_column_headings() {
  $output =  '<th style="width: 80px">Pool type</th>';
  $output .= '<th>Titles</th>';
  $output .= '<th>Categories</th>';
  $output .= '<th>Description</th>';
  $output .= '<th style="text-align: right; width: 40px">Nid</th>';
  $output .= '<th style="text-align: right; width: 40px">Media</th>';
  $output .= '<th style="text-align: right; width: 40px">Published</th>';
  
  return $output;
}

function _mediapool_statistics_generate_row($pool, $terms) {
  $output =  '<td class="mediapool mediapool-data mediapool-type">' . $pool->type . '</td>';
  $output .= '<td class="mediapool mediapool-data mediapool-title">' . $pool->title . '</td>';
  $output .= '<td class="mediapool mediapool-data mediapool-categories">' . $terms . '</td>';
  $output .= '<td class="mediapool mediapool-data mediapool-body">' . $pool->body . '</td>';
  $output .= '<td class="mediapool mediapool-data mediapool-nid" style="text-align: right">' . $pool->nid . '</td>';
  $output .= '<td class="mediapool mediapool-data mediapool-media" style="text-align: right">' . mediapool_count_media($pool->nid,'imagepool_image') . '</td>';
  $output .= '<td class="mediapool mediapool-data mediapool-published" style="text-align: center"><input type="checkbox" name="cb' . $pool->nid . '"' . (($pool->status) ? ' checked' : '') . ' /></td>';
  
  return $output;
}
