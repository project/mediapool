<?php

function imagepool_settings_form($form_state) {
//  drupal_add_css(drupal_get_path('module', 'imagepool') .'/css/imagepool.css');
  $form = (empty($form_state['hold']['values'])) ? _imagepool_settings_form($form_state) : _imagepool_fields_form($form_state);
  return $form; 
}

function _imagepool_settings_form() {
  $form = array();
  $types = node_get_types('names');
  if(isset($types['imagepool'])) unset($types['imagepool']);
  $form['imagepool_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Use ImagePool With'),
    '#default_value' => variable_get('imagepool_node_types', $types),
    '#options' => $types,
    '#description' => '<p>'. t('Select the content types for which ImagePool 
      support should be available.'
      ) .'</p>',
    );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Next'),
    );

  return $form;
}

function imagepool_settings_form_validate($form, &$form_state) {
 if ($form_state['clicked_button']['#value'] == 'Back') {
   unset($form_state['hold']);
   $form_state['rebuild'] = TRUE;
 }
}

function imagepool_settings_form_submit($form, &$form_state) {
   if (empty($form_state['hold']['values']) && $form_state['clicked_button']['#value'] == 'Next') {
	  $vid = imagepool_get_vid();
      $before = array_filter(variable_get('imagepool_node_types', array()));
      $after = array_filter($form_state['values']['imagepool_node_types']);
      $del = array_diff($before, $after);
      $add = array_diff($after, $before);
      variable_set('imagepool_node_types', $after);
      $form_state['hold']['values'] = $form_state['values'];
      $form_state['rebuild'] = true;
	  // update the vocabulary to reflect the content types that use it
	  db_query("DELETE FROM {vocabulary_node_types} WHERE vid=" . $vid . " AND type!='imagepool'");
	  foreach ($after as $nodetype) {
	  	db_query("INSERT INTO {vocabulary_node_types} (vid, type) VALUES (" . $vid . ",'" . $nodetype . "')");
	  }
    }
   else {
      $before = array();
      $query = db_query("SELECT name,value FROM {variable} WHERE name LIKE '%imagepool_node_field_%'"); 
      if ($query) {
        while ($result = db_fetch_object($query)) {
          $before[$result->name] = $result->value;
        }
      }
      $after = array();
      if (isset($form_state['values']['imagepool_fields'])) {
        foreach ($form_state['values']['imagepool_fields'] as $key => $fld) {
          $after[$key] = $fld;
        }
      }    
      $del = array_diff($before, $after);
      $add = array_diff($after, $before);
      foreach ($after as $key => $data) {
        variable_set('imagepool_node_field_' . $key, $data);
      }
      drupal_set_message(t('Settings updated.'));
      drupal_goto('admin/settings/mediapool');
   }
}

function _imagepool_fields_form($form_state) {
//  drupal_add_css(drupal_get_path('module', 'imagepool') .'/css/imagepool.css');
  $form = array();
  $form['heading'] = array (
    '#type' => 'markup',
    '#value' => t('<p>Enter the name of the image field to be controlled for each content type</p>'),
  );
  $form['imagepool_fields'] = array(
    '#tree' => TRUE,
  );
// make a list of the image fields for this content type
  foreach($form_state['hold']['values']['imagepool_node_types'] as $data) {
    if ($data) {
      $query = db_query("SELECT field_name FROM {content_node_field_instance} WHERE widget_type='imagefield_widget' AND type_name = '" . $data . "'");
      $options = array();
      if ($query) {
        while ($result = db_fetch_object($query)) {
          $options[$result->field_name] = $result->field_name;
        }
        if ($options) {
          $options[''] = t('Please select');
          $form['imagepool_fields'][$data] = array(
            '#type' => 'select',
            '#title' => $data,
            '#default_value' => variable_get('imagepool_node_field_'.$data, ''),
            '#options' => $options,
          );
        }
        else {
          $form[$data] = array(
            '#type' => 'markup',
          '#value' => "<strong>" . $data . ":</strong><br />" . t('There is no image field defined for this content type') . '<p />',
          );
        }
      }
    }
  }
  $form['prevpage'] = array(
    '#type' => 'button',
    '#value' => t('Back'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  unset($form_state['hold']);
  return $form;
}
